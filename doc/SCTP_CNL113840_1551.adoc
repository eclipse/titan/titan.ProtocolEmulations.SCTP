---
Author: Gábor Szalai
Version: 1551-CNL 113 840, Rev. PB1
Date: 2018-08-02
---
= SCTP Engine for TTCN-3 Toolset with TITAN, Function Description
:author: Gábor Szalai
:revnumber: 1551-CNL 113 840, Rev. PB1
:revdate: 2018-08-02
:toc:

= Introduction

== How to Read this Document

This is the Function Specification for the SCTP engine. The SCTP engine is developed for the TTCN-3 Toolset with TITAN. This document should be read together with the <<_3,Product Revision Information>>.

== Scope

The purpose of this document is to specify the content of the SCTP engine. The document is primarily addressed to the end users of the product. Basic knowledge of <<_2,TTCN-3>> and <<_4,TITAN TTCN-3 Test Executor>> is valuable when reading this document.

== References

1. [[_1]]RFC 4960 +
Stream Control Transmission Protocol

2. [[_2]]ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3; Part 1: Core Language

3. [[_3]]109 21-CNL 113 840-1 Uen +
SCTP Engine for TTCN-3 Toolset with TITAN, Product Revision Information

4. [[_4]]1/198 17-CRL 113 200/5 Uen +
User Guide for the TITAN TTCN-3 Test Executor

== Abbreviations

TTCN-3:: Testing and Test Control Notation version 3

SCTP:: Stream Control Transmission Protocol

== Terminology

No specific terminology is used.

== System Requirements

The SCTP Engine is a set of TTCN-3 source code files that can be used as part of TTCN-3 test suites only. Hence, SCTP engine alone does not put specific requirements on the system used. However, in order to compile and execute a TTCN-3 test suite using the set of protocol modules, the following system requirements must be satisfied:

* TITAN TTCN-3 Test Executor CRL 113 200/5 R5A or higher installed. For installation guide, see <<_2,here>>.

= SCTP Engine

== Overview

The SCTP engine implements a complete SCTP stack. This allows run tests over SCTP.

image::images/Picture1.png[]

The SCTP engine is a standalone TTCN-3 component, which implements all of the functionality of the SCTP stack. The SCTP API is offered via the test ports:

* `SCTP_Engine_API_request_PT`: The SCTP users sends request to the SCTP Engine via it.
* `SCTP_Engine_API_indication_PT`: The SCTP Engine informs the SCTP users about the SCTP events.

The encoded SCTP packets are exchanged via the `SCTP_Engine_packet_PT` test port.

== Installation

The set of protocol modules can be used in developing TTCN-3 test suites using any text editor. However, to make the work more efficient, a TTCN-3-enabled text editor is recommended (e.g. nedit, xemacs). Since the SCTP protocol is used as part of a TTCN-3 test suite, this requires TTCN-3 Test Executor to be installed before the module can be compiled and executed together with other parts of the test suite. For more details on the installation of TTCN-3 Test Executor, see the relevant section of the <<_4,User Guide for the TITAN TTCN-3 Test Executor>>.

== Configuration

See the `SCTP_Engine_Definition.ttcn`

= Functional specification

== Supported SCTP functionalities

The following RFCs are supported:

RFC4960 (Except multi-homing), RFC6525, draft-ietf-tsvwg-sctp-ndata-07

The SCTP engine can handle several SCTP clients. Because there is no direct connection between the transport layer and the SCTP Engine, each transport channel is identified by the `transport_id` parameters of the API calls and test port messages.

== Start up

The SCTP engine is implemented as a standalone TTCN-3 component.

The start-up procedure:

1.  Create `SCTP_Engine_CT` type component
2.  Start the `SCTP_Core_main()` function on the component
3.  Connect the transport mapper functions/component to the `p_packet_port` test port of the component
4.  Connect the SCTP users to the `p_sctp_req_api` and `p_sctp_ind_api` test ports of the component

== Association establishment

The `transport_id` should be specified during the association establishment.

In order to create a listening SCTP endpoint, call the `S_SCTP_Listen` on the test port connected to `p_sctp_req_api`.

The SCTP Engine will notify the user in the case of incoming association establishment by calling the `S_SCTP_Connected_ind` on `p_sctp_ind_api`.

To establish outgoing SCTP association, call the `S_SCTP_Connect` on the test port connected to `p_sctp_req_api`.

== Data exchange

Call `S_SCTP_Send_req` to send data.

The SCTP Engine will call `S_SCTP_Received_ind` when data arrived from the peer.

== Association shutdown

The shutdown procedure and the release of the `assoc_id` should be done as:

User initiated shutdown is a 3 step procedure:

1.  call `S_SCTP_Shutdown`
2.  receive `S_SCTP_Shutdown_ind`
3.  call `S_SCTP_Shutdown_conf`

Remote side initiated shutdown:

1.  receive `S_SCTP_Shutdown_ind`
2.  call `S_SCTP_Shutdown_conf`

The `assoc_id` is released only after the call of the `S_SCTP_Shutdown_conf`.

== SCTP packet transport

The SCTP engine sends the encoded outgoing SCTP packet via `p_packet_port` test port. The user specified transport mapper is responsible to send the sctp packet to the SUT.

The sctp packet from SUT should be sent to the SCTP Engine via the same test port.
